<?php

function elastic_preprocess_html(&$variables){
    
    if (!empty($variables['page']['featured'])){
        $variables['classes_array'][] = 'featured';   
    }
    
  if (!empty($variables['page']['elastic_one'])
    || !empty($variables['page']['elastic_two'])
    || !empty($variables['page']['elastic_three'])
    || !empty($variables['page']['elastic_four'])) {
    $variables['classes_array'][] = 'elastic-content-columns';
  }    
    //Parking space reserved for bootstrap tasks
    
}