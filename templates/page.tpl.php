<!--
- Elastic is most attractive when you allow your content to occupy
- the central region.  The basic framework is the following:
- [			HEADER REGION				]
- [								]
- [		ELASTIC BAND/CONTENT WRAP			]
- [ 	SIDEBAR1|	(CONTENT, optional fluid regions)	]
- [	SIDEBAR2|	(fluid region wrap area)		]
- [		END OF ELASTIC BAND/CONTENT-WRAP		]
- [								]
- [			FOOTER REGION				]

-->


<div id="page-wrap"><div id="page">

<!--HEADER REGION-->
<div id="header" class="<?php print $secondary_menu ? 'with-secondary-menu': 'without-secondary-menu'; ?>"
  <div class="clearfix">
    
    <?php unset($page['header']['system_main-menu']);
    //preventing double print of links, but leaving the render
    //for sake of easy re-enable
    ?>

    <div id="header-logo-name-slogan">
      <?php if ($logo): ?>
	<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
	  <div id="header-logo">
	  <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
	  </div>
	</a>
      <?php endif; ?>
      <?php if ($site_name || $site_slogan): ?>
	<div id="header-name-slogan">
	  <?php if ($site_name): ?>
	    <div id="header-site-name">
	      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="site_name">
	      <?php print ($site_name); ?>
	      </a>
	    </div>
	  <?php endif; ?>
	  <?php if ($site_slogan): ?>
	    <div id="header-slogan">
	      <?php print ($site_slogan); ?>
	    </div>
	  <?php endif; ?>
	</div>
      <?php endif; ?> 
      <?php if ($messages): ?>
	<div id="messages"><div class="clearfix">
	  <?php print $messages; ?>
	</div></div><!--/#messages -->
      <?php endif; ?>
    </div><!--header-logo-name-slogan-->

    <div id="drupal-header">     
      <?php print render($page['header']); ?>
    </div>
      
    <div id="drupal-menu-blocks">  
      <?php if ($main_menu): ?>
	<div id="drupal-main-menu">
	    <?php print theme('links__system_main_menu',
	      array(
		'links' => $main_menu,
		'attributes' => array(
				    'id' => 'drupal-main-menu-links',
				    'class' => array('links', 'inline', 'clearfix'),
				),
		'heading' => array(
				  'text' => t('Main Menu'),
				  'level' => 'h2',
				  'class' => array('element-invisible'),
			    ),
	      )
	    );?>
	</div> <!--/#drupal-main-menu-->
      <?php endif; ?>
      <?php if ($secondary_menu): ?>
	<div id="secondary-menu">
	  <?php print theme('links__system_secondary_menu',
	    array(
	      'links' => $secondary_menu,
	      'attributes' => array(
				  'id' => 'drupal-secondary-menu-links',
				  'class' => array('links', 'inline','clearfix'),
			      ),
	      'heading' => array(
				'text' => t('Secondary Menu'),
				'level' => 'h2',
				'class' => array('element-invisible'),
				),
	    )
	  );?>
	</div> <!-- /#secondary-menu -->
      <?php endif; ?>
    </div><!--drupal menu blocks-->
      
    <?php if ($page['featured']): ?>
      <div id="featured"><div class="clearfix">
	<?php print render($page['featured']); ?>
      </div></div><!--/#featured -->
    <?php endif; ?>
  </div><!--.clearfix-->
</div><!--#header ?.$secondary menu-->
<!--/HEADER REGION-->
<div class="gap"></div>
<!--BODY REGION-->
<div id="main-wrapper" class="clearfix">
  <div id="main" class="clearfix">
    <?php if ($page['sidebar_first'] || $page['sidebar_second']): ?>
      <div id="sidebar-container">
	<?php if ($page['sidebar_first']): ?>
	  <div id="sidebar-first" class="sidebar">
	    <?php print render($page['sidebar_first']); ?>    
	  </div>
	<?php endif; ?>
	<?php if ($page['sidebar_second']): ?>
	  <div id="sidebar-second" class="column sidebar">
	    <?php print render($page['sidebar_second']); ?>    
	  </div>
	<?php endif; ?>	
      </div><!--sidebar-container-->  
    <?php endif; ?><p>

    <div id="content">   
      <?php if ($breadcrumb): ?>
	<div id="breadcrumb"><?php print $breadcrumb; ?></div>
      <?php endif; ?>
      
      <?php if ($page['highlighted']): ?>
	<div id="highlighted"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h1 class="title" id="page-title">
          <?php print $title; ?>
        </h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      
      <?php if ($tabs): ?>
        <div class="tabs">
          <?php print render($tabs); ?>
        </div>
      <?php endif; ?>
      
      <?php print render($page['help']); ?>
      
      <?php if ($action_links): ?>
        <ul class="action-links">
          <?php print render($action_links); ?>
        </ul>
      <?php endif; ?>

      <?php print render($page['content']); ?>

      <?php if ($page['elastic_one'] || $page['elastic_two'] || $page['elastic_three'] || $page['elastic_four']): ?>
	<div id="stretch">

	  <?php if ($page['elastic_one']): ?>
	  <div class="elastic"><?php print render($page['elastic_one']); ?> </div>
	  <?php endif; ?>   
	  <?php if ($page['elastic_two']): ?>
	  <div class="elastic"><?php print render($page['elastic_two']); ?> </div>
	  <?php endif; ?>  
	  <?php if ($page['elastic_three']): ?>	  
	  <div class="elastic"><?php print render($page['elastic_three']); ?> </div>
	  <?php endif; ?>
	  <?php if ($page['elastic_four']): ?>
	  <div class="elastic"><?php print render($page['elastic_four']); ?> </div>
	  <?php endif; ?>
        </div><!--/#stretch-->
      <?php endif; ?>

	<?php print $feed_icons; ?>   

    </div> <!--/#content -->
    

  </div><!--/main-->
</div><!--/main-wrapper-->
<!--/BODY REGION-->
<div class="gap"></div>


<!--FOOTER REGION-->
  <div id="footer-wrapper">
    <?php if ($page['elastic_footer_one'] || $page['elastic_footer_two'] || $page['elastic_footer_three']): ?>
      <div id="elastic-footer">
	<?php if ($page['elastic_footer_one']): ?>
	  <div class="elastic-footer-column"><?php print render($page['elastic_footer_one']); ?></div>
	<?php endif; ?>
	<?php if ($page['elastic_footer_two']): ?>
	  <div class="elastic-footer-column"><?php print render($page['elastic_footer_two']); ?></div>
	<?php endif; ?>
	<?php if ($page['elastic_footer_three']): ?>
	  <div class="elastic-footer-column"><?php print render($page['elastic_footer_three']); ?></div>
	<?php endif; ?>
      </div> <!-- /#footer-columns -->
    <?php endif; ?>

    <?php if ($page['footer']): ?>
      <div id="footer" class="clearfix">
        <?php print render($page['footer']); ?>
      </div> <!-- /#footer -->
    <?php endif; ?>

  </div></div> <!--/#footer-wrapper -->

<!--/FOOTER REGION-->
  
</div></div><!--#page, #page-wrap-->

    